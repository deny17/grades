<nav class="pagination-wrap">
    <ul class="pagination justify-content-center">
        <li class="page-item <?=$page == 1 ? 'disabled' : ''?>">
            <a class="page-link" href="/?<?=http_build_query(array_merge($_GET, array("page" => $page - 1)));?>" tabindex="-1">&#8592;</a>
        </li>
        <?php for ($i = 1; $i <= $pages; $i++) {?>
            <li class="page-item <?=$page == $i ? 'active' : ''?>">
                <a class="page-link" href="/?<?=http_build_query(array_merge($_GET, array("page" => $i)));?>"><?=$i?></a>
            </li>
        <?php }?>
        <li class="page-item <?=$page >= $pages ? 'disabled' : ''?>">
            <a class="page-link" href="/?<?=http_build_query(array_merge($_GET, array("page" => $page + 1)));?>">&#8594;</a>
        </li>
    </ul>
</nav>
