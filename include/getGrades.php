<?php

$connect = getConnection($host, $user, $password, $db);

$page = intval(isset($_GET['page']) ? $_GET['page'] : 1);

$limit = intval(isset($_GET['count']) ? $_GET['count'] : 10);;
$offset = $limit * ($page - 1);

$lines = getCountPages($connect)['count'];
$pages = ceil($lines / $limit);

$courseNames = getCourseNames($connect, $limit, $offset);

foreach ($courseNames as $courseName) {
    $lessonNames[$courseName] = getLessonNames($connect, $courseName);
    $courseGrades[$courseName]['grade'] =  getCourseGrades($connect, $courseName)['grade'];
    $courseGrades[$courseName]['numbers'] =  getCourseGrades($connect, $courseName)['numbers'];

    foreach ($lessonNames[$courseName] as $lessonName) {
        $lessonGrades[$courseName][$lessonName] = getLessonGrades($connect, $courseName, $lessonName);
    }

    array_multisort($lessonGrades[$courseName], SORT_DESC);
    // array_multisort($courseGrades, SORT_DESC);
    $courseGrades = array_reverse($courseGrades);
}

mysqli_close($connect);
