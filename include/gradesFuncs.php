<?php

include $_SERVER['DOCUMENT_ROOT'] . '/include/db.php';
include $_SERVER['DOCUMENT_ROOT'] . '/include/getConnection.php';

// Получаем количество страниц
function getCountPages($connect)
{
    $result = mysqli_query(
        $connect,
        "SELECT COUNT(DISTINCT course_name) as count FROM big_grades LIMIT 1"
    );

    return mysqli_fetch_assoc($result);
}


// Получаем уникальные названия курсов
function getCourseNames($connect, $limit,$offset)
{
    $result = mysqli_query(
        $connect,
        "SELECT DISTINCT course_name FROM big_grades LIMIT $limit OFFSET $offset"
    );

    $courseNames = [];

    while ($value = mysqli_fetch_assoc($result)) {
        $courseNames[] = $value['course_name'];
    }

    return $courseNames;
}

// Получаем уникальные названия уроков
function getLessonNames($connect, $courseName)
{
    $result = mysqli_query(
        $connect,
        "SELECT DISTINCT lesson_name FROM big_grades WHERE course_name = '$courseName' ORDER BY lesson_name"
    );

    $lessonNames = [];

    while ($value = mysqli_fetch_assoc($result)) {
        // $lessonNames[$courseName] = $value['lesson_name'];
        $lessonNames[] = $value['lesson_name'];

    }

    return $lessonNames;
}

// Получаем среднюю оценку по курсу
function getCourseGrades($connect, $courseName)
{
    $result = mysqli_query(
        $connect,
        "SELECT AVG(value) as grade, COUNT(*) as numbers FROM big_grades WHERE course_name = '$courseName' LIMIT 1"
    );

    return mysqli_fetch_assoc($result);
}

// Получаем среднюю оценку по уроку
function getLessonGrades($connect, $courseName, $lessonName)
{
    $result = mysqli_query(
        $connect,
        "SELECT AVG(value) as grade, COUNT(*) as numbers FROM big_grades WHERE course_name = '$courseName' and lesson_name = '$lessonName'"
    );

    return mysqli_fetch_assoc($result);
}