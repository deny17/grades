<?php

$host = 'localhost';
$user = 'root';
$password = 'root';
$db = 'grades';

// Create connection
$con = new mysqli($host, $user, $password, $db);

// Check connection
if ($con->connect_error) {
    echo "Connection failed: " . $con->connect_error;
}
else {
    echo "";
}