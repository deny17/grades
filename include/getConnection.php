<?php

function getConnection($host, $user, $password, $db)
{
    static $connection;
    if ($connection === null) {
        $connection = mysqli_connect($host, $user, $password, $db);
    }
    return $connection;
}
