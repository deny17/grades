<?php

include $_SERVER['DOCUMENT_ROOT'] . '/include/gradesFuncs.php';
include $_SERVER['DOCUMENT_ROOT'] . '/include/getGrades.php';

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.6.0.slim.js"
            integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY="
            crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
<style>
    .wrap {
        max-width: 600px !important;
        margin: 0px auto !important;
    }

    table {
        display: none;
    }

    h1 {
        margin: 25px;
    }

    h4 {
        background: #fdfdfd !important;
    }

    .pagination-wrap {
        margin: 20px !important;
    }

</style>

<div class="wrap">
    <h1 class="text-center">Курсы</h1>

    <div class="text-right m-3">показать:
        <a href="/?<?=http_build_query(array_merge($_GET, array("count" => "10", "page" => 1)));?>" class="badge badge-pill <?=$_GET['count'] == 10 ? 'badge-secondary' : 'badge-light'?>">10</a>
        <a href="/?<?=http_build_query(array_merge($_GET, array("count" => "20", "page" => 1)));?>" class="badge badge-pill <?=$_GET['count'] == 20 ? 'badge-secondary' : 'badge-light'?>">20</a>
        <a href="/?<?=http_build_query(array_merge($_GET, array("count" => "50", "page" => 1)));?>" class="badge badge-pill <?=$_GET['count'] == 50 ? 'badge-secondary' : 'badge-light'?>">50</a>
        <a href="/?<?=http_build_query(array_merge($_GET, array("count" => "100", "page" => 1)));?>" class="badge badge-pill <?=$_GET['count'] == 100 ? 'badge-secondary' : 'badge-light'?>">100</a>
        <a href="/?<?=http_build_query(array_merge($_GET, array("count" => $lines, "page" => 1)));?>" class="badge badge-pill <?=$_GET['count'] == $lines ? 'badge-secondary' : 'badge-light'?>">все</a>
    </div>

    <?php foreach ($courseGrades as $name=>$courseName) {?>
    <h4 class="list-group-item list-group-item-action d-flex justify-content-between d-inline-flex" id="<?=$name?>">
            <?=$name?>
            <div>
                <span class="badge badge-success badge-pill"><?=round($courseName['grade'],2)?></span>
                <span class="badge badge-primary badge-pill"><?=$courseName['numbers']?></span>
            </div>
    </h4>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center">Название урока</th>
            <th class="text-center" >Оценка</th>
            <th class="text-center" >Количество</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($lessonGrades[$name] as $lessonName=>$lesson) {?>
        <tr>
            <td style="width: 500px"><?=$lessonName?></td>
            <td class="text-center" style="width: 100px"><?=round($lesson['grade'],2)?></td>
            <td class="text-center" style="width: 100px"><?=round($lesson['numbers'],2)?></td>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <?php }?>

    <?php include $_SERVER['DOCUMENT_ROOT'] . '/templates/pagination.php';?>
</div>

<script>
    $(document).ready(function() {
        $("h4").click(function() {
            $(this).next().toggle(500);
        });
    });
</script>

</body>
</html>
